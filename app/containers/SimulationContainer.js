import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Api from '../../lib/core-api-client/ApiV1';
import PageCaption from '../components/page/PageCaption';
import SimulacraControl from '../components/simulator/SimulacraControl';
import Simulacra0 from '../components/simulator/Simulacra';
import ApiError from '../components/indicators/ApiError';
import EventMapper from '../components/mapper/EventMapper';
import Message from '../components/messages/Message';

export default class SimulationContainer extends Component {
  static propTypes = {
    api: PropTypes.instanceOf(Api),
    simulation: PropTypes.shape({ ticks: PropTypes.number }),
    simulation_announcements: PropTypes.arrayOf(PropTypes.string),
    onAnnouncement: PropTypes.func
  }

  static defaultProps = {
    api: null,
    simulation: { ticks: 0 },
    simulation_announcements: [],
    onAnnouncement: () => { }
  }

  constructor(props) {
    super(props);

    this.state = {
      actions: [],
      events: []
    };
  }

  componentDidMount() {
    this.props.api.fetchEvents()
      .then(data => {
        const acts = new Simulacra0().actions(data);
        this.setState({ actions: acts, events: data });

        return 1;
      })
      .catch(error => console.log(error));
  }

  handleTurnGateOn = (action) => {
    console.info('gateOn', action);
    this.fireUpTheGate(action.event, 'before_departion');
  }

  handleArchive = (action) => {
    console.info('archive', action);
  }

  handleReturn = (action) => {
    console.info('return', action);
    this.setEventStatus(action.event, action);
    this.fireUpTheGate(action.event, 'before_return');
  }

  handleStatusChange = (action) => {
    console.info('status', action);

    this.setEventStatus(action.event, action);
  }

  setEventStatus = (event, action) => {
    // Be careful with these values
    const statusIdMap = {
      set_status_boarding: 2,
      set_status_departed: 3,
      show_return: 4,
      set_status_returned: 5
    }[action.do];

    const ev = event;
    ev.eventStatusId = statusIdMap;
    const modifiedEvent = EventMapper.unmap(ev);

    this.props.api
      .updateEvent(modifiedEvent)
      .then(result => Message.show(`Event status has been updated [${result.id}].`))
      // .then(() => this.handleRefresh())
      .catch(error => ApiError(error));
  }

  fireUpTheGate = (evt, tpy) => {
    this.props.api
      .proxy({ name: 'fire_gate', event: evt, type: tpy })
      .then(() => Message.show(`Firing up the Gate #${evt.gateId}.`))
      .catch(error => ApiError(error));
  }

  render() {
    const { simulation, simulation_announcements, onAnnouncement } = this.props;
    const announcements = simulation_announcements.length > 0 ?
      simulation_announcements.map((a, i) => <div key={a + i}>{`${i + 1} - ${a}`}</div>) : <div>Empty</div>;

    return (
      <div>
        <PageCaption text="02 Simulation (WIP)" />
        <div className="pt-callout">
          Here you can run the system simulation.
          Be aware it will change statuses of events.
        </div>
        <div>
          <div>
            Queue for announcements:
          {announcements}
          </div>
        </div>
        <SimulacraControl
          actions={this.state.actions}
          events={this.state.events}
          simulacra={simulation}
          onAnnouncement={onAnnouncement}
          onTurnGateOn={this.handleTurnGateOn}
          onArchive={this.handleArchive}
          onReturn={this.handleReturn}
          onStatusChange={this.handleStatusChange}
        />
      </div>
    );
  }
}
