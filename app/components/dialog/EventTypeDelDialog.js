import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dialog, Colors } from '@blueprintjs/core';

import EventTypePropType from '../../props/EventTypePropType';
import RefsPropType from '../../props/RefsPropType';
import LocalePropType from '../../props/LocalePropType';
import L18n from '../../components/l18n/L18n';

/**
 * The class for event type add dialog.
 *
 * @since 0.1.0
 */
export default class EventTypeDelDialog extends Component {
  static propTypes = {
    types: PropTypes.arrayOf(EventTypePropType),
    refs: RefsPropType.isRequired,
    trans: LocalePropType.isRequired,
    callback: PropTypes.func.isRequired
  }

  static defaultProps = {
    types: [],
    callback: () => { }
  }

  constructor(props) {
    super(props);

    this.state = { isOpen: false, types: [], refs: {}, tr: {} };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState({ types: nextProps.refs.types, refs: nextProps.refs, tr: nextProps.trans });
    }
  }

  toggleDialog = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  onDelete = (id) => {
    this.props.callback(id, this.onSuccess);
  }

  onSuccess = () => {
    this.toggleDialog();
  }

  render() {
    if (!this.state.types) {
      return null;
    }

    const l18n = new L18n(this.state.tr, this.state.refs);
    const eventTypes = this.state.types.map(op =>
      (<div key={op.id}>
        {l18n.findTranslationById(op, 'i18nEventTypeName')}&nbsp;/&nbsp;{l18n.findTranslationById(op, 'i18nEventTypeSubname')}
        <button type="button" className="pt-button pt-minimal pt-icon-remove pt-intent-danger" onClick={this.onDelete.bind(this, op.id)} />
      </div>)
    );

    return (
      <Dialog isOpen={this.state.isOpen} onClose={this.toggleDialog} canOutsideClickClose={false} title="Delete an event type">
        <div className="pt-dialog-body">
          <div className="pt-callout">
            Click on the button (<span className="pt-icon-remove" />) bellow to delete right away an event type.
            <div style={{ color: Colors.RED1 }}>
              The application does not allow to delete event types
              which are used in existing events.
            </div>
          </div>
          <p>&nbsp;</p>
          {eventTypes}
        </div>
      </Dialog>
    );
  }
}
