
export default class Action {
  constructor(event, time, do_, weight) {
    this.event = event;
    this.time = time;
    this.do = do_;
    this.weight = weight;
  }
}
