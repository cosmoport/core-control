# core-control
> 0.1.3

### Uses
- Electron
- React
- Blueprint

### Submodules
- core-api-client

In order to update all repository submodules you have to execute this command:

```text
git submodule foreach git pull
```